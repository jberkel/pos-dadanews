#!/usr/bin/env python
# coding=utf8

from os import environ
environ['AELIUS_DATA'] = 'aelius_data'
environ['NLTK_DATA'] = 'nltk_data'
import codecs
from Aelius import AnotaCorpus, Toqueniza
from random import choice


HEADLINES = codecs.open('headlines-pt.txt', 'r', 'UTF-8').readlines()
POS_TO_SWAP = ['ADJ', 'ADJ-F-P', 'ADJ-F', 'NPR', 'VB']


def annotate(s):
    return AnotaCorpus.anota_sentencas([Toqueniza.TOK_PORT.tokenize(s)], AnotaCorpus.TAGGER2)


def build_pos_tags(lines):
    pos_tags = {}
    for line in lines:
        for (word, tag) in annotate(line)[0]:
            if tag not in pos_tags:
                pos_tags[tag] = set()
            pos_tags[tag].add(word)
    return pos_tags


def swap_pos(tags, pos, token):
    if pos in POS_TO_SWAP and pos in tags:
        return choice(list(tags[pos] - set(token)))
    else:
        return token


def recombine_headlines(tags, lines):
    return map(lambda line: (line, map(lambda (token, pos): swap_pos(tags, pos, token), annotate(line)[0])),
               lines)


if __name__ == "__main__":
    n = len(HEADLINES)
    tags = build_pos_tags(HEADLINES[0:n])
    for (original, recombined) in (recombine_headlines(tags, HEADLINES[0:n])):
        print original
        print " ".join(recombined)
        print "-" * 80
