# pos-dadanews

A version of [dadanews][] implemented with a POS tagger.

## Run

    $ pip install PyYAML
    $ ./pos_dada_news.py


[dadanews]: https://github.com/rlafuente/dadanews
